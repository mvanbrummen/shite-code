package util

import org.scalatestplus.play._


class HashUtilSpec extends PlaySpec {

  "Hash util must" should {
    "correctly perform a MD5 hash" in {
      HashUtil.md5Hash("test") mustBe "098F6BCD4621D373CADE4E832627B4F6".toLowerCase
    }
  }

}
