package dal

import java.sql.Date

import models.PostLikes
import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile

trait PostLikesMapping {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  protected lazy val postLikes = TableQuery[PostLikesTable]

  class PostLikesTable(tag: Tag) extends Table[PostLikes](tag, "post_likes") {

    def id = column[Long]("post_likes_id", O.PrimaryKey, O.AutoInc)

    def userId = column[Long]("user_id")

    def postId = column[Long]("post_id")

    def vote = column[Boolean]("vote")

    def createdDate = column[Date]("created_date")

    def modifiedDate = column[Date]("modified_date")

    def * = (id, userId, postId, vote, createdDate, modifiedDate) <> ((PostLikes.apply _).tupled, PostLikes.unapply)

  }

}