package dal

import java.sql.Date

import models.Comment
import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile

trait CommentMapping {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  protected lazy val comments = TableQuery[CommentTable]

  class CommentTable(tag: Tag) extends Table[Comment](tag, "comment") {

    def id = column[Long]("comment_id", O.PrimaryKey, O.AutoInc)

    def postId = column[Long]("post_id")

    def userId = column[Long]("user_id")

    def body = column[String]("body")

    def createdDate = column[Date]("created_date")

    def modifiedDate = column[Date]("modified_date")

    def * = (id, userId, postId, body, createdDate, modifiedDate) <> ((Comment.apply _).tupled, Comment.unapply)

  }

}