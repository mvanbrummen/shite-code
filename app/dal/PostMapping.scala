package dal

import java.sql.Date

import models.Post
import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile

trait PostMapping {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  protected lazy val posts = TableQuery[PostTable]

  class PostTable(tag: Tag) extends Table[Post](tag, "post") {

    def id = column[Long]("post_id", O.PrimaryKey, O.AutoInc)

    def userId = column[Long]("user_id")

    def title = column[String]("title")

    def description = column[String]("description")

    def views = column[Long]("views")

    def codeSnippet = column[String]("code_snippet")

    def createdDate = column[Date]("created_date")

    def modifiedDate = column[Date]("modified_date")

    def * = (id, userId, title, description, views, codeSnippet, createdDate, modifiedDate) <> ((Post.apply _).tupled, Post.unapply)

  }

}
