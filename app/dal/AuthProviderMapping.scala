package dal

import java.sql.Date

import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile

case class AuthProvider(id: Long, provider: String, createdDate: Date, modifiedDate: Date)

trait AuthProviderMapping {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  protected lazy val authProviders = TableQuery[AuthProviderTable]

  class AuthProviderTable(tag: Tag) extends Table[AuthProvider](tag, "auth_provider") {

    def id = column[Long]("auth_provider_id", O.PrimaryKey, O.AutoInc)

    def provider = column[String]("provider", O.Unique)

    def createdDate = column[Date]("created_date")

    def modifiedDate = column[Date]("modified_date")

    def * = (id, provider, createdDate, modifiedDate) <> ((AuthProvider.apply _).tupled, AuthProvider.unapply)

  }

}