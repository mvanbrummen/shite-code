package dal

import java.sql.Date

import models.User
import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile

trait UserMapping {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  protected lazy val users = TableQuery[UserTable]

  class UserTable(tag: Tag) extends Table[User](tag, "user") {

    def id = column[Long]("user_id", O.PrimaryKey, O.AutoInc)

    def name = column[Option[String]]("name")

    def username = column[String]("username")

    def emailAddress = column[Option[String]]("email_address")

    def passwordHash = column[Option[String]]("password_hash")

    def passwordSalt = column[Option[String]]("password_salt")

    def authProviderId = column[Option[Long]]("auth_provider_id")

    def authProvider = column[Option[Long]]("auth_provider")

    def status = column[Boolean]("status")

    def createdDate = column[Date]("created_date")

    def modifiedDate = column[Date]("modified_date")

    def * = (id, name, username, emailAddress, passwordHash, passwordSalt, authProviderId, authProvider, status, createdDate, modifiedDate) <> ((User.apply _).tupled, User.unapply)

  }

}