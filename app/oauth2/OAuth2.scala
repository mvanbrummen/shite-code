package oauth2

import play.api.libs.ws._

import scala.concurrent.{ExecutionContext, Future}


case class OAuth2Configuration(clientId: String,
                               clientSecret: String,
                               callbackUrl: String,
                               authUrl: String,
                               tokenUrl: String,
                               userInfoUrl: String)

abstract class OAuth2[T](config: OAuth2Configuration, ws: WSClient)
                        (implicit ec: ExecutionContext){
  def user(body: String): T

  import config._

  def signInUrl = s"$authUrl?client_id=$clientId&redirect_uri=$callbackUrl"

  def requestAccessToken(code: String, additionalParams: Seq[(String, String)]): Future[Option[String]] = {
    val queryParams = Seq(
      "client_id" -> clientId,
      "client_secret" -> clientSecret,
      "code" -> code,
      "redirect_uri" -> callbackUrl
    ) ++ additionalParams

    ws.url(tokenUrl)
      .addHttpHeaders("Accept" -> "application/json")
      .addQueryStringParameters(queryParams:_*).post("").map { resp =>

      (resp.json \ "access_token").asOpt[String]
    }
  }

  def authenticate(code: String, additionalParams: (String, String)*): Future[T] = for {
    maybeToken <- requestAccessToken(code, additionalParams)
    userInfo <- requestUserInfo(maybeToken.getOrElse(throw new RuntimeException))
  } yield userInfo

  def requestUserInfo(accessToken: String): Future[T] = {
    ws.url(userInfoUrl)
      .addQueryStringParameters("access_token" -> accessToken)
      .get.map(r => user(r.body))
  }

}
