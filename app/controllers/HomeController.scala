package controllers

import javax.inject._

import play.api.mvc._
import services.PostService

import scala.concurrent.ExecutionContext

case class ThumbnailPost(id: Long,
                         title: String,
                         codeSnippet: String,
                         username: String,
                         uploaded: String)

@Singleton
class HomeController @Inject()(cc: ControllerComponents, postService: PostService)(implicit ec: ExecutionContext)
  extends AbstractController(cc) {

  import util.DateUtil.sqlToString

  def index(page: Option[Int]) = Action.async { implicit request: Request[AnyContent] =>
    postService.findAllPage(page).map { page =>
      val postDatas = page.results.map { case (post, user) =>
        ThumbnailPost(post.postId,
          post.title,
          post.codeSnippet.split("\n").take(4).mkString("\n"),
          user.username,
          post.createdDate)
      }
      Ok(views.html.index(postDatas, page.page, page.count, page.pageCount))
    }
  }

}
