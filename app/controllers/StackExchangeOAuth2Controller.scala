package controllers

import javax.inject.{Inject, Singleton}

import models.AuthProviderEnum
import oauth2.{OAuth2, OAuth2Configuration}
import play.api.Configuration
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Json, Reads}
import play.api.libs.ws.WSClient
import play.api.mvc.{AbstractController, AnyContent, ControllerComponents, Request}
import services.UserService
import util.Constants._

import scala.concurrent.{ExecutionContext, Future}

case class StackExchangeUser(id: Long,
                             username: String,
                             email: Option[String],
                             avatar_url: String,
                             web_url: String,
                             name: Option[String])

@Singleton
class StackExchangeOAuth2Controller @Inject()(cc: ControllerComponents,
                                              config: Configuration,
                                              userService: UserService,
                                              ws: WSClient)(implicit ec: ExecutionContext) extends AbstractController(cc) {


  val STACKEXCHANGE = new OAuth2[StackExchangeUser](OAuth2Configuration(
    config.get[String]("oauth2.stackexchange.client.id"),
    config.get[String]("oauth2.stackexchange.client.secret"),
    config.get[String]("oauth2.stackexchange.callback.url"),
    config.get[String]("oauth2.stackexchange.authorize.url"),
    config.get[String]("oauth2.stackexchange.access.token.url"),
    config.get[String]("oauth2.stackexchange.api.user.url")
  ), ws) {

    override def user(body: String): StackExchangeUser = Json.parse(body).validate.getOrElse(throw new RuntimeException)

  }

  implicit val stackExchangeUserReads: Reads[StackExchangeUser] = (
    (JsPath \\ "user_id").read[Long] and
      (JsPath \\ "display_name").read[String] and
      (JsPath \\ "email").readNullable[String] and
      (JsPath \\ "profile_image").read[String] and
      (JsPath \\ "link").read[String] and
      (JsPath \\ "name").readNullable[String]
    ) (StackExchangeUser.apply _)

  def signIn() = Action.async {
    Future.successful(Redirect(STACKEXCHANGE.signInUrl))
  }

  def callback(code: Option[String]) = Action.async { implicit request: Request[AnyContent] =>
    code.map { c =>
      for {
        user <- STACKEXCHANGE.authenticate(c)
        userId <- userService.createUserWithAuthProvider(user.username, user.email, user.id, AuthProviderEnum.STACKEXCHANGE)
      } yield {
        Redirect(routes.HomeController.index(None))
          .withSession(
            SessionUserId -> userId.toString,
            SessionUserName -> user.username,
            SessionEmailAddress -> user.email.getOrElse(""),
            SessionProfileLink -> user.web_url,
            SessionUserAvatar -> user.avatar_url)
      }
    } getOrElse (throw new RuntimeException("No auth code"))
  }

}
