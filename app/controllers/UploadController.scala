package controllers

import javax.inject._

import play.api.data.Form
import play.api.data.Forms._
import play.api.i18n._
import play.api.libs.Files
import play.api.mvc._
import services.PostService
import util.{Constants, UserAuth}

import scala.concurrent.{ExecutionContext, Future}


case class UploadData(title: String, description: String, codeSnippet: String)

@Singleton
class UploadController @Inject()(cc: ControllerComponents, postService: PostService)(implicit ec: ExecutionContext)
  extends AbstractController(cc) with I18nSupport with UserAuth {

  val uploadForm = Form(
    mapping(
      "title" -> nonEmptyText,
      "description" -> nonEmptyText,
      "codeSnippet" -> nonEmptyText
    )(UploadData.apply)(UploadData.unapply)
  )

  def get = Action { implicit request: Request[AnyContent] =>
    if (isUserAuthed()) Ok(views.html.upload(uploadForm)) else Redirect(routes.LoginController.loginPage(Some(true)))
  }

  def post() = Action(parse.multipartFormData).async { implicit request: Request[MultipartFormData[Files.TemporaryFile]] =>
    def failure(formWithErrors: Form[UploadData]) = {
      Future.successful(BadRequest(views.html.upload(formWithErrors)))
    }

    def success(uploadData: UploadData) = {
      request.session.get(Constants.SessionUserId).map { id =>
        postService.insertOne(uploadData.title, uploadData.description, uploadData.codeSnippet, id.toLong).map { id =>
          Redirect(routes.PostController.post(id))
        }.recover {
          case _ => BadRequest(views.html.upload(uploadForm))
        }
      }.getOrElse {
        Future.successful(Unauthorized("User is unauthenticated"))
      }

    }

    uploadForm.bindFromRequest.fold(failure, success)
  }

}
