package controllers


import javax.inject._

import models.Comment
import play.api.mvc.{AbstractController, AnyContent, ControllerComponents, Request}
import services.{CommentService, PostLikesService, PostService}
import util._
import play.api.libs.json._

import scala.concurrent.{ExecutionContext, Future}

case class PostData(postId: Long, title: String, description: String, views: Long, codeSnippet: String,
                    username: String, likes: Int, dislikes: Int, uploaded: String)

case class CommentData(username: String, emailHash: String, comment: String, createdDate: String, modifiedDate: String)

@Singleton
class PostController @Inject()(cc: ControllerComponents,
                               postService: PostService,
                               postLikesService: PostLikesService,
                               commentService: CommentService)
                              (implicit ec: ExecutionContext) extends AbstractController(cc) {

  import DateUtil.sqlToString

  implicit val commentDataWrites = new Writes[CommentData] {
    def writes(c: CommentData) = Json.obj(
      "username" -> c.username,
      "emailHash" -> c.emailHash,
      "comment" -> c.comment,
      "createdDate" -> c.createdDate,
      "modifiedDate" -> c.modifiedDate
    )
  }

  def post(id: Long) = Action.async { implicit request: Request[AnyContent] =>
    postService.findOneWithUser(id).flatMap {

      case Some((post, user)) => {
        postService.update(post.postId, post.copy(views = post.views + 1))

        postLikesService.getVotes(user.userId).map {
          case (likes, dislikes) =>
            Ok(views.html.post(PostData(post.postId, post.title, post.description, post.views, post.codeSnippet, user.username, likes, dislikes, post.createdDate)))
        }
      }

      case None => Future.successful(NotFound("Can't find user"))
    }
  }

  def getComments(id: Long) = Action.async { implicit request: Request[AnyContent] =>
    fetchComments(id)
  }

  def postComment(postId: Long) = Action.async { implicit request: Request[AnyContent] =>
    request.session.get("user_id").map { id =>
      val reqBody = request.body.asFormUrlEncoded
      val comment = reqBody.map(_.head).get._2.head
      val userId = id.toString.toLong

      commentService.insert(Comment(userId = userId, postId = postId, body = comment))

      fetchComments(postId)
    }.getOrElse {
      Future.successful(Unauthorized("User is unauthenticated"))
    }
  }

  private def fetchComments(id: Long) = {
    commentService.findByPostId(id).map { comment =>
      val comments = comment.map { case (c, u) =>
        // TODO avatar for oauth user
        CommentData(u.username, HashUtil.md5Hash(u.emailAddress.getOrElse("")), c.body, c.createdDate, c.modifiedDate)
      }

      Ok(Json.toJson(comments))
    }
  }

}
