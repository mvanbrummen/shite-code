package controllers

import javax.inject._

import play.api.libs.json._
import play.api.mvc.{AbstractController, AnyContent, ControllerComponents, Request}
import services.PostLikesService

import scala.concurrent.{ExecutionContext, Future}


@Singleton
class PostLikesController @Inject()(cc: ControllerComponents, postLikesService: PostLikesService)(implicit ec: ExecutionContext)
  extends AbstractController(cc) {

  def vote(id: Long) = Action.async { request: Request[AnyContent] =>

    request.session.get("user_id").map { userId =>
      val reqBody = request.body.asFormUrlEncoded
      val vote = reqBody.map(_.head).get._2.head.toBoolean

      postLikesService.vote(id, userId.toLong, vote).map { case (likes, dislikes) =>
        Ok(Json.obj(
          "likes" -> JsNumber(likes),
          "dislikes" -> JsNumber(dislikes))
        )
      }
    }.getOrElse {
      Future.successful(Unauthorized("User is unauthenticated"))
    }
  }

}
