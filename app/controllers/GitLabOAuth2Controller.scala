package controllers

import javax.inject.{Inject, Singleton}

import models.AuthProviderEnum
import oauth2.{OAuth2, OAuth2Configuration}
import play.api.Configuration
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Json, Reads}
import play.api.libs.ws.WSClient
import play.api.mvc.{AbstractController, AnyContent, ControllerComponents, Request}
import services.UserService
import util.Constants._

import scala.concurrent.{ExecutionContext, Future}

case class GitLabUser(id: Long,
                      username: String,
                      email: Option[String],
                      avatar_url: String,
                      web_url: String,
                      name: Option[String])

@Singleton
class GitLabOAuth2Controller @Inject()(cc: ControllerComponents,
                                       config: Configuration,
                                       userService: UserService,
                                       ws: WSClient)(implicit ec: ExecutionContext) extends AbstractController(cc) {


  val GITLAB = new OAuth2[GitLabUser](OAuth2Configuration(
    config.get[String]("oauth2.gitlab.client.id"),
    config.get[String]("oauth2.gitlab.client.secret"),
    config.get[String]("oauth2.gitlab.callback.url"),
    config.get[String]("oauth2.gitlab.authorize.url"),
    config.get[String]("oauth2.gitlab.access.token.url"),
    config.get[String]("oauth2.gitlab.api.user.url")
  ), ws) {

    override def user(body: String): GitLabUser = Json.parse(body).validate.getOrElse(throw new RuntimeException)

    override def signInUrl: String = super.signInUrl.concat("&response_type=code")

  }

  implicit val gitLabUserReads: Reads[GitLabUser] = (
    (JsPath \ "id").read[Long] and
      (JsPath \ "username").read[String] and
      (JsPath \ "email").readNullable[String] and
      (JsPath \ "avatar_url").read[String] and
      (JsPath \ "web_url").read[String] and
      (JsPath \ "name").readNullable[String]
    ) (GitLabUser.apply _)

  def signIn() = Action.async {
    Future.successful(Redirect(GITLAB.signInUrl))
  }

  def callback(code: Option[String]) = Action.async { implicit request: Request[AnyContent] =>
    code.map { c =>
      for {
        user <- GITLAB.authenticate(c, "grant_type" -> "authorization_code")
        userId <- userService.createUserWithAuthProvider(user.username, user.email, user.id, AuthProviderEnum.GITLAB)
      } yield {
        Redirect(routes.HomeController.index(None))
          .withSession(
            SessionUserId -> userId.toString,
            SessionUserName -> user.username,
            SessionEmailAddress -> user.email.getOrElse(""),
            SessionProfileLink -> user.web_url,
            SessionUserAvatar -> user.avatar_url)
      }
    } getOrElse (throw new RuntimeException("No auth code"))
  }

}
