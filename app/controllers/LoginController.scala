package controllers

import javax.inject._

import play.api.Logger
import play.api.data.Form
import play.api.data.Forms._
import play.api.i18n._
import play.api.libs.Files
import play.api.libs.ws._
import play.api.mvc._
import services._
import util.Constants._
import util.HashUtil

import scala.concurrent.{ExecutionContext, Future}

case class RegisterData(name: String, username: String, emailAddress: String, password: String, termsAgreement: Boolean)

case class LoginData(emailAddress: String, password: String)

@Singleton
class LoginController @Inject()(cc: ControllerComponents,
                                userService: UserService,
                                ws: WSClient)(implicit ec: ExecutionContext) extends AbstractController(cc)
  with I18nSupport {

  val logger = Logger(this.getClass)

  val registerForm = Form(
    mapping(
      "name" -> text(1, 250),
      "username" -> text(1, 50),
      "emailAddress" -> email,
      "password" -> text(8, 128),
      "termsAgreement" -> boolean
    )(RegisterData.apply)(RegisterData.unapply)
  )

  val loginForm = Form(
    mapping(
      "emailAddress" -> email,
      "password" -> nonEmptyText
    )(LoginData.apply)(LoginData.unapply)
  )

  def loginPage(signUp: Option[Boolean]) = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.login(registerForm, loginForm, signUp))
  }

  def login() = Action(parse.multipartFormData).async { implicit request: Request[MultipartFormData[Files.TemporaryFile]] =>
    def failure(formWithErrors: Form[LoginData]) = {
      Future.successful(BadRequest(views.html.login(registerForm, formWithErrors, Some(false))))
    }

    def success(loginData: LoginData) = {

      userService.authUser(loginData.emailAddress, loginData.password).map { maybeUser =>
        maybeUser.map { user =>
          logger.debug(s"User authed successfully ${user.userId} ${user.username}")

          buildUserSession(user.username, user.emailAddress.get, user.userId.toLong)
        } getOrElse BadRequest("nope")
      }.recover {
        case _ => BadRequest(views.html.login(registerForm, loginForm, Some(false)))
      }
    }

    loginForm.bindFromRequest().fold(failure, success)
  }

  def register() = Action(parse.multipartFormData).async { implicit request: Request[MultipartFormData[Files.TemporaryFile]] =>
    def failure(formWithErrors: Form[RegisterData]) = {
      Future.successful(BadRequest(views.html.login(formWithErrors, loginForm, Some(true))))
    }

    def success(registerData: RegisterData) = {
      userService.createUser(registerData.name, registerData.username, registerData.password, registerData.emailAddress).map { id =>
        logger.debug(s"User created successfully $id")

        buildUserSession(registerData.username, registerData.emailAddress, id)
      }.recover {
        case _ => BadRequest(views.html.login(registerForm, loginForm, Some(true)))
      }
    }

    registerForm.bindFromRequest.fold(failure, success)
  }

  private def buildUserSession(username: String, emailAddress: String, id: Long) = {
    Redirect(routes.HomeController.index(None))
      .withSession(
        SessionUserId -> id.toString,
        SessionUserName -> username,
        SessionEmailAddress -> emailAddress,
        SessionProfileLink -> "#",
        SessionUserAvatar -> s"https://www.gravatar.com/avatar/${HashUtil.md5Hash(emailAddress)}?d=identicon")
  }

  def logout() = Action { implicit request: Request[AnyContent] =>
    Redirect(routes.HomeController.index(None)).withNewSession
  }

}
