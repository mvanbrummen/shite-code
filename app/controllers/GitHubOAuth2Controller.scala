package controllers

import javax.inject.{Inject, Singleton}

import models.AuthProviderEnum
import oauth2.{OAuth2, OAuth2Configuration}
import play.api.Configuration
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Json, Reads}
import play.api.libs.ws.WSClient
import play.api.mvc.{AbstractController, AnyContent, ControllerComponents, Request}
import services.UserService
import util.Constants._

import scala.concurrent.{ExecutionContext, Future}

case class GitHubUser(id: Long,
                      login: String,
                      email: Option[String],
                      avatar_url: String,
                      html_url: String,
                      name: Option[String])

@Singleton
class GitHubOAuth2Controller @Inject()(cc: ControllerComponents,
                                       config: Configuration,
                                       userService: UserService,
                                       ws: WSClient)(implicit ec: ExecutionContext) extends AbstractController(cc) {


  val GITHUB = new OAuth2[GitHubUser](OAuth2Configuration(
    config.get[String]("oauth2.github.client.id"),
    config.get[String]("oauth2.github.client.secret"),
    config.get[String]("oauth2.github.callback.url"),
    config.get[String]("oauth2.github.authorize.url"),
    config.get[String]("oauth2.github.access.token.url"),
    config.get[String]("oauth2.github.api.user.url")
  ), ws) {

    override def user(body: String): GitHubUser = Json.parse(body).validate.getOrElse(throw new RuntimeException)

    override def signInUrl: String = super.signInUrl.concat("&scope=user")

  }

  implicit val gitHubUserReads: Reads[GitHubUser] = (
    (JsPath \ "id").read[Long] and
      (JsPath \ "login").read[String] and
      (JsPath \ "email").readNullable[String] and
      (JsPath \ "avatar_url").read[String] and
      (JsPath \ "html_url").read[String] and
      (JsPath \ "name").readNullable[String]
    ) (GitHubUser.apply _)

  def signIn() = Action.async {
    Future.successful(Redirect(GITHUB.signInUrl))
  }

  def callback(code: Option[String]) = Action.async { implicit request: Request[AnyContent] =>
    code.map { c =>
      for {
        user <- GITHUB.authenticate(c)
        userId <- userService.createUserWithAuthProvider(user.login, user.email, user.id, AuthProviderEnum.GITHUB)
      } yield {
        Redirect(routes.HomeController.index(None))
          .withSession(
            SessionUserId -> userId.toString,
            SessionUserName -> user.login,
            SessionEmailAddress -> user.email.getOrElse(""),
            SessionProfileLink -> user.html_url,
            SessionUserAvatar -> user.avatar_url)
      }
    } getOrElse (throw new RuntimeException("No auth code"))
  }

}
