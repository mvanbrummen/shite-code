package services

import java.sql.Date
import java.util.Calendar
import javax.inject.{Inject, Singleton}

import dal.{PostMapping, UserMapping}
import models.{Post, User}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

case class Page(results: Seq[(Post, User)], offset: Int, page: Int, count: Int, pageCount: Int)


trait PostService {
  def findAll(): Future[Seq[Post]]

  def findOne(id: Long): Future[Option[Post]]

  def findOneWithUser(id: Long): Future[Option[(Post, User)]]

  def insertOne(title: String, description: String, codeSnippet: String, userId: Long): Future[Long]

  def update(postId: Long, post: Post): Future[Unit]

  def findAllPage(page: Option[Int]): Future[Page]
}

@Singleton
class PostServiceImpl @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext)
  extends HasDatabaseConfigProvider[JdbcProfile] with PostMapping with UserMapping with PostService {

  import profile.api._

  override def findAll(): Future[Seq[Post]] = db.run(posts.result)

  override def insertOne(title: String, description: String, codeSnippet: String, userId: Long): Future[Long] = {
    val now = new Date(Calendar.getInstance().getTimeInMillis)
    val action = (posts returning posts.map(_.id)) += Post(1L, userId, title, description, 0, codeSnippet, now, now)
    db.run(action)
  }

  override def findOne(id: Long): Future[Option[Post]] = db.run(posts.filter(_.id === id).result.headOption)

  override def findOneWithUser(id: Long): Future[Option[(Post, User)]] = {
    val q = for {
      p <- posts.filter(_.id === id)
      u <- users if p.userId === u.id
    } yield (p, u)

    db.run(q.result.headOption)
  }

  override def update(postId: Long, post: Post): Future[Unit] = {
    val postToUpdate = post.copy(postId = postId)
    db.run(posts.filter(_.id === postId).update(postToUpdate)).map(_ => ())
  }

  def findAllPage(page: Option[Int]): Future[Page] =
    page.map { p => findAllPage(page = p) } getOrElse findAllPage()

  private def findAllPage(page: Int = 0, pageSize: Int = 24): Future[Page] = {
    val offset = page * pageSize

    val q = (for {
      (post, user) <- posts join users on (_.userId === _.id)
    } yield (post, user))
      .drop(offset)
      .take(pageSize)

    for {
      count <- count()
      results <- db.run(q.result)
    } yield Page(results, offset, page, count, (count + pageSize - 1) / pageSize)
  }

  private def count(): Future[Int] = db.run(posts.length.result)
}
