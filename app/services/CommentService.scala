package services

import javax.inject.{Inject, Singleton}

import dal.{CommentMapping, UserMapping}
import models.{Comment, User}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

trait CommentService {

  def insert(comment: Comment): Future[Unit]

  def findByPostId(postId: Long): Future[Seq[(Comment, User)]]
}

@Singleton
class CommentServiceImpl @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext)
  extends HasDatabaseConfigProvider[JdbcProfile] with CommentService with CommentMapping with UserMapping {

  import profile.api._

  override def insert(comment: Comment): Future[Unit] =
    db.run(comments += comment).map(_ => ())

  override def findByPostId(postId: Long): Future[Seq[(Comment, User)]] = {
    val q = (for {
      c <- comments.filter(_.postId === postId)
      u <- users if c.userId === u.id
    } yield (c, u)).sortBy(_._1.createdDate.desc)

    db.run(q.result)
  }
}
