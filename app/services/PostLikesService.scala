package services

import javax.inject.{Inject, Singleton}

import dal.{PostLikesMapping, PostMapping, UserMapping}
import models.PostLikes
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

trait PostLikesService {
  def getVotes(postId: Long): Future[(Int, Int)]

  def vote(postId: Long, userId: Long, vote: Boolean): Future[(Int, Int)]

}

@Singleton
class PostLikesServiceImpl @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext)
  extends HasDatabaseConfigProvider[JdbcProfile] with PostLikesService with PostMapping with UserMapping with PostLikesMapping {

  import profile.api._

  override def getVotes(postId: Long): Future[(Int, Int)] =
    for {
      p <- db.run(postLikes.filter(_.postId === postId).result)
    } yield (p.count(_.vote), p.count(!_.vote))

  override def vote(postId: Long, userId: Long, vote: Boolean): Future[(Int, Int)] = {
    incrementVote(postId, userId, vote)
    getVotes(postId)
  }

  private def incrementVote(postId: Long, userId: Long, vote: Boolean): Unit =
    db.run(postLikes.filter(p => p.postId === postId && p.userId === userId).result.headOption).map {
      case Some(post) =>
        if (vote == post.vote) {
          db.run(postLikes.filter(_.id === post.postLikesId).delete)
        } else {
          db.run(postLikes.filter(_.id === post.postLikesId).map(_.vote).update(vote))
        }

      case None => {
        val action = (postLikes returning postLikes.map(_.id)) += PostLikes(postLikesId = 1L, userId = userId, postId = postId, vote = vote)
        db.run(action)
      }
    }
}
