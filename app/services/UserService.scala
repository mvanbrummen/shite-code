package services

import java.sql.Date
import java.util.Calendar
import javax.inject.{Inject, Singleton}

import dal.{AuthProviderMapping, UserMapping}
import models.AuthProviderEnum.AuthProviderEnum
import models.User
import org.mindrot.jbcrypt.BCrypt
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

trait UserService {
  def findAll(): Future[Seq[User]]

  def findOne(id: Long): Future[Option[User]]

  def insertOne(name: String, username: String, emailAddress: String, passwordHash: String, passwordSalt: String): Future[Long]

  def authUser(emailAddress: String, password: String): Future[Option[User]]

  def createUser(name: String, username: String, password: String, emailAddress: String): Future[Long]

  def createUserWithAuthProvider(username: String, emailAddress: Option[String], authProviderId: Long, authProvider: AuthProviderEnum): Future[Long]
}

@Singleton
class UserServiceImpl @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext)
  extends HasDatabaseConfigProvider[JdbcProfile] with UserMapping with UserService with AuthProviderMapping {

  import profile.api._

  override def findAll(): Future[Seq[User]] =  db.run(users.result)

  override def insertOne(name: String, username: String, emailAddress: String, passwordHash: String, passwordSalt: String): Future[Long] = {
    val now = new Date(Calendar.getInstance().getTimeInMillis)
    val action = (users returning users.map(_.id)) += User(1L, Some(name), username, Some(emailAddress), Some(passwordHash), Some(passwordSalt), None, None, true, now, now)
    db.run(action)
  }

  override def findOne(id: Long): Future[Option[User]] = db.run(users.filter(_.id === id).result.headOption)

  override def authUser(emailAddress: String, password: String): Future[Option[User]] = {
    for {
      maybeUser <- db.run(users.filter(_.emailAddress === emailAddress).result.headOption)
      if BCrypt.checkpw(password, maybeUser.get.passwordHash.get)
    // TODO check user is active too
    } yield maybeUser
  }

  override def createUser(name: String, username: String, password: String, emailAddress: String): Future[Long] = {
    val salt = BCrypt.gensalt()
    val hash = BCrypt.hashpw(password, salt)

    insertOne(name, username, emailAddress, hash, salt)
  }

  override def createUserWithAuthProvider(username: String, emailAddress: Option[String], authProviderId: Long, authProvider: AuthProviderEnum): Future[Long] = {
    for {
      provider <- db.run(authProviders.filter(_.provider === authProvider.toString).result.headOption)
      id <- insertOneWithAuthProvider(username, emailAddress, authProviderId, provider.get.id)
    } yield id
  }

  private def insertOneWithAuthProvider(username: String, emailAddress: Option[String], authProviderId: Long, authProvider: Long): Future[Long] = {
    val now = new Date(Calendar.getInstance().getTimeInMillis)
    val action = (users returning users.map(_.id)) += User(1L, None, username, emailAddress, None, None, Some(authProviderId), Some(authProvider), true, now, now)
    db.run(action)
  }

}
