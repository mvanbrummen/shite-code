package util

import java.security.MessageDigest

import org.apache.commons.codec.binary.Hex


object HashUtil {

  def md5Hash(s: String): String =  Hex.encodeHexString(MessageDigest.getInstance("MD5").digest(s.getBytes)).toLowerCase

}
