package util

import java.sql.Date
import java.text.SimpleDateFormat


object DateUtil {

  implicit def sqlToString(date: Date): String = {
    val df = new SimpleDateFormat("dd MMM yyyy")

    df format date
  }

}
