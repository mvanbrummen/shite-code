package util


object Constants {
  val SessionUserId = "user_id"
  val SessionUserName = "user_name"
  val SessionEmailAddress = "email_address"
  val SessionProfileLink = "profile_link"
  val SessionUserAvatar = "user_avatar"
}
