package util

import play.api.mvc.{AnyContent, Request}


trait UserAuth {

  def isUserAuthed()(implicit request: Request[AnyContent]): Boolean = {
    request.session.get(Constants.SessionUserId).isDefined
  }

}
