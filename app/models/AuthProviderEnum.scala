package models


object AuthProviderEnum extends Enumeration {
  type AuthProviderEnum = Value
  val GITHUB, GITLAB, STACKEXCHANGE = Value
}
