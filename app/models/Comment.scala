package models

import java.sql.Date
import java.util.Calendar


case class Comment(commentId: Long = 1L,
                   userId: Long,
                   postId: Long,
                   body: String,
                   createdDate: Date = new Date(Calendar.getInstance().getTimeInMillis),
                   modifiedDate: Date = new Date(Calendar.getInstance().getTimeInMillis))
