package models

import java.sql.Date


case class User(userId: Long,
                name: Option[String],
                username: String,
                emailAddress: Option[String],
                passwordHash: Option[String],
                passwordSalt: Option[String],
                authProviderId: Option[Long],
                authProvider: Option[Long],
                status: Boolean,
                createdDate: Date,
                modifiedDate: Date)


