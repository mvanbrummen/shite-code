package models

import java.sql.Date
import java.util.Calendar

case class PostLikes(postLikesId: Long,
                     userId: Long,
                     postId: Long,
                     vote: Boolean,
                     createdDate: Date = new Date(Calendar.getInstance().getTimeInMillis),
                     modifiedDate: Date = new Date(Calendar.getInstance().getTimeInMillis))
