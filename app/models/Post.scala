package models

import java.sql.Date


case class Post(postId: Long,
                userId: Long,
                title: String,
                description: String,
                views: Long,
                codeSnippet: String,
                createdDate: Date,
                modifiedDate: Date)
