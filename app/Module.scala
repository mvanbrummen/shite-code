import com.google.inject.AbstractModule
import services._

class Module extends AbstractModule {
  override def configure() = {
    bind(classOf[PostService]).to(classOf[PostServiceImpl])
    bind(classOf[PostLikesService]).to(classOf[PostLikesServiceImpl])
    bind(classOf[UserService]).to(classOf[UserServiceImpl])
    bind(classOf[CommentService]).to(classOf[CommentServiceImpl])
  }
}
