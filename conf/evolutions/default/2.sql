# --- !Ups

INSERT INTO "user" (name, username, email_address, password_hash, password_salt, status)
VALUES ('Michael Van Brummen', 'mvanbrummen', 'michaelvanbrummen@gmail.com', '$2a$10$Nu7XKI5R1nJ3aIc1c6CrcOvFpnbYpMnKDW3pi5oLs6kLYdh8to366', '$2a$10$Nu7XKI5R1nJ3aIc1c6CrcO', true);

INSERT INTO auth_provider (provider)
VALUES ('GITHUB');

INSERT INTO auth_provider (provider)
VALUES ('GITLAB');

INSERT INTO auth_provider (provider)
VALUES ('STACKEXCHANGE');

# --- !Downs

DELETE FROM "user";