# --- !Ups

CREATE TABLE auth_provider (
    auth_provider_id SERIAL PRIMARY KEY,
    provider VARCHAR(50),
    created_date timestamp,
    modified_date timestamp
);

ALTER TABLE auth_provider ALTER COLUMN created_date SET DEFAULT now();
ALTER TABLE auth_provider ALTER COLUMN modified_date SET DEFAULT now();

CREATE TABLE "user" (
  user_id SERIAL PRIMARY KEY,
  name VARCHAR(250),
  username VARCHAR(50) NOT NULL,
  email_address VARCHAR(254),
  password_hash VARCHAR,
  password_salt VARCHAR,
  auth_provider_id INTEGER UNIQUE,
  auth_provider INTEGER DEFAULT NULL,
  status BOOLEAN,
  created_date timestamp,
  modified_date timestamp,
  CONSTRAINT user_auth_provider_fk FOREIGN KEY (auth_provider) REFERENCES auth_provider(auth_provider_id)
);

ALTER TABLE "user" ALTER COLUMN created_date SET DEFAULT now();
ALTER TABLE "user" ALTER COLUMN modified_date SET DEFAULT now();

CREATE TABLE post (
    post_id SERIAL PRIMARY KEY,
    user_id INTEGER,
    title VARCHAR(50) NOT NULL,
    description VARCHAR(250) NOT NULL,
    views INTEGER DEFAULT 0,
    code_snippet TEXT NOT NULL,
    created_date timestamp,
    modified_date timestamp,
    CONSTRAINT post_user_id_fk FOREIGN KEY (user_id) REFERENCES "user"(user_id)
);

ALTER TABLE post ALTER COLUMN created_date SET DEFAULT now();
ALTER TABLE post ALTER COLUMN modified_date SET DEFAULT now();

CREATE TABLE comment (
    comment_id SERIAL PRIMARY KEY,
    post_id INTEGER,
    user_id INTEGER,
    body VARCHAR(250),
    created_date timestamp,
    modified_date timestamp,
    CONSTRAINT comment_post_id_fk FOREIGN KEY (post_id) REFERENCES post(post_id),
    CONSTRAINT comment_user_id_fk FOREIGN KEY (user_id) REFERENCES "user"(user_id)
);

ALTER TABLE comment ALTER COLUMN created_date SET DEFAULT now();
ALTER TABLE comment ALTER COLUMN modified_date SET DEFAULT now();

CREATE TABLE post_likes (
    post_likes_id SERIAL PRIMARY KEY,
    user_id INTEGER,
    post_id INTEGER,
    vote boolean,
    created_date timestamp,
    modified_date timestamp,
    CONSTRAINT post_likes_id_fk FOREIGN KEY (post_id) REFERENCES post(post_id),
    CONSTRAINT post_likes_user_id_fk FOREIGN KEY (user_id) REFERENCES "user"(user_id),
    CONSTRAINT post_likes_user_post_uc UNIQUE (user_id, post_id)
);

ALTER TABLE post_likes ALTER COLUMN created_date SET DEFAULT now();
ALTER TABLE post_likes ALTER COLUMN modified_date SET DEFAULT now();

# --- !Downs

DROP TABLE auth_provider;
DROP TABLE "user";
DROP TABLE post;
DROP TABLE post_likes;
DROP TABLE comment;
