$(function() {
    $('[data-toggle="tooltip"]').tooltip()

    let searchBtn = $('#searchBtn');
    let searchInput = $('#searchInput');

    searchBtn.click(function() {
        searchInput.toggle('fast');

        if (searchBtn.parent().hasClass('active')) {
            searchBtn.parent().removeClass('active');
        } else {
           searchBtn.parent().addClass('active');
           searchInput.val('');
           searchInput.focus();
        }
    });

});